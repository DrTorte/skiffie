﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Skiffie.Models;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Skiffie.Controllers
{
    [Authorize]
    public class AppointmentsController : Controller
    {
        private SkiffieContext db = new SkiffieContext();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private UserManager<ApplicationUser> manager = new UserManager<ApplicationUser>(
            new UserStore<ApplicationUser>(
                new SkiffieContext()));

        // GET: Appointmensttd
        public ActionResult Index()
        {
            var appointments = db.Appointments.Include(a => a.Customer)
                .Include(a => a.TimeSlot).OrderBy(a=>a.Date).ThenBy(a=>a.TimeSlot.Name).ThenBy(a=>a.Customer.Name);
            return View(appointments.ToList());
        }

        public ActionResult GetAppointmentList(bool showOpen = true, bool showClosed = false, bool upcomingOnly = false, int count = 12, int skip = 0)
        {
            string userid = User.Identity.GetUserId();
            List<Appointment> appointments = db.Appointments.Include(a => a.Customer)
                   .Include(a => a.TimeSlot).Include(a=>a.FieldTech).OrderBy(a => a.Date).ThenByDescending(a=>a.FieldTech.UserID == userid).ThenBy(a => a.TimeSlot.Name).ThenBy(a => a.Customer.Name).ToList();

            List<AppointmentTableView> apps = appointments.Select(x => new AppointmentTableView(x)).ToList();

            //this only shows the ones with dates from today or onwards.
            if (upcomingOnly)
            {
                DateTime Now = DateTime.Now.AddDays(-1);
                apps = apps.Where(a => a.Date >= Now).ToList();
            }

            if(!showOpen)
            {
                apps = apps.Where(x => x.CurrentState != Appointment.AppointmentState.Dispatched &&
                    x.CurrentState != Appointment.AppointmentState.NeedsReschedule &&
                    x.CurrentState != Appointment.AppointmentState.Open &&
                    x.CurrentState != Appointment.AppointmentState.PendingAssignment &&
                    x.CurrentState != Appointment.AppointmentState.PendingCompletion &&
                    x.CurrentState != Appointment.AppointmentState.PendingDispatch).ToList();
            }
            if (!showClosed)
            {
                apps = apps.Where(x => x.CurrentState != Appointment.AppointmentState.Cancelled &&
                    x.CurrentState != Appointment.AppointmentState.Completed &&
                    x.CurrentState != Appointment.AppointmentState.Failed).ToList();
            }

            //after filtering, take the amount count dictated.
            apps = apps.Skip(skip).Take(count).ToList();

            foreach (var x in apps)
            {
                x.Classes = "AppointmentRow";
                if (x.FieldTech != null && x.FieldTech.UserID == userid)
                {
                    if (x.Date.ToString("yyyy-MM-dd") == DateTime.Now.ToString("yyyy-MM-dd"))
                    {
                        x.Classes += " success";
                    }
                    else if (x.Date <= DateTime.Today.AddDays(-1) && x.CurrentState == Appointment.AppointmentState.Open)
                    {
                        x.Classes += " danger";
                    }
                    else
                    {
                        x.Classes += " warning";
                    }
                } else if (x.Date.ToString("yyyy-MM-dd") == DateTime.Now.ToString("yyyy-MM-dd"))
                {
                    x.Classes += " info";
                } 
            }

            ViewBag.UserID = userid;
            return PartialView("_AppointmentsTable", apps);
        }

        #region View/Data.
        // GET: Appointments/View/5
        public ActionResult View(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Appointment appointment = db.Appointments.Find(id);
            if (appointment == null)
            {
                return HttpNotFound();
            }

            AppointmentViewModel app = new AppointmentViewModel(appointment);
            ViewBag.Actions = ActionButtons(app);
            return View(app);
        }

        //Get the selected field tech, or, if you are an admin, a list to select them. For dt/dd view... probably don't need that specific though, heh.
        public ActionResult FieldTechDD(int id)
        {
            Appointment app = db.Appointments.Find(id);
            if (app == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //return two different views. One for admins, one for non-admins.
            if (manager.FindById(User.Identity.GetUserId()).SkiffieRoleId == db.SkiffieRoles.First(x => x.Name == "Admin").SkiffieRolesId)
            {
                AppointmentViewModel appView = new AppointmentViewModel(app);
                List<SelectListItem> myList = new List<SelectListItem>();
                foreach (var tech in db.FieldTechs.Where(x=>x.Active))
                {
                    var userTech = manager.FindById(tech.UserID);
                    var userName = userTech.Name;
                    if (tech.FieldTechID == app.FieldTechID)
                    {
                        appView.TechsId = tech.FieldTechID;
                    }
                    myList.Add(new SelectListItem
                    {
                        Selected = tech.FieldTechID == app.FieldTechID,
                        Value = tech.FieldTechID.ToString(),
                        Text = userName
                    });
                }
                // admin view here.

                appView.Techs = myList;

                return PartialView("_FieldTechDDAdmin", appView);
            }
            else
            {
                if (app.FieldTechID == null)
                {
                    ViewBag.Name = "Unassigned";
                }
                else
                {
                    ViewBag.Name = manager.FindById(app.FieldTech.UserID).Name;
                }
                return PartialView("_FieldTechDD");
            }
        }

        //get buttons for the view field.
        private List<NavigationBars> ActionButtons(AppointmentViewModel appointment)
        {
            //add actions that are available depending on the state of the appointment.
            //this depends on the access the agent has.
            var identity = (ClaimsIdentity)User.Identity;
            List<NavigationBars> MyActions = new List<NavigationBars>();

            
            var rank = identity.Claims.DefaultIfEmpty(null).FirstOrDefault(x => x.Type == ("UserRole")).Value;

            if (appointment.CurrentState == Appointment.AppointmentState.Cancelled ||
                appointment.CurrentState == Appointment.AppointmentState.Completed ||
                appointment.CurrentState == Appointment.AppointmentState.Failed)
            {
                if (rank == "Admin")
                {
                    MyActions.Add(
                        new NavigationBars
                        {
                            Action = "Delete",
                            Name = "Delete",
                            Controller = "Assignments"
                        });
                }
            }
            else if (appointment.CurrentState == Appointment.AppointmentState.Dispatched ||
                appointment.CurrentState == Appointment.AppointmentState.Open ||
                appointment.CurrentState == Appointment.AppointmentState.PendingAssignment ||
                appointment.CurrentState == Appointment.AppointmentState.PendingCompletion ||
                appointment.CurrentState == Appointment.AppointmentState.PendingDispatch)
            {
                MyActions.AddRange(new List<NavigationBars>{
                    new NavigationBars{
                        Action = "Cancel",
                        Name = "Cancel",
                        Controller = "Assignments"
                    },
                    new NavigationBars
                    {
                        Action="Reschedule",
                        Name="Reschedule",
                        Controller="Assignments"
                    }
                });

                if (rank == "Field Tech" || rank == "Admin")
                {
                    MyActions.AddRange(new List<NavigationBars> {
                        new NavigationBars{
                            Action="Fail",
                            Name="Fail / Reject",
                            Controller="Assignments"
                        },
                        new NavigationBars{
                            Action="Reject",
                            Name="Reject",
                            Controller="Assignments"
                        },
                        new NavigationBars{
                            Action="Complete",
                            Name="Complete",
                            Controller="Assignments"
                        }
                      });
                }
            }

            return MyActions;
        }
        #endregion
        #region Create
        // GET: Appointments/Create
        public ActionResult Create()
        {
            ViewBag.CustomerID = new SelectList(db.Customers, "CustomerID", "Name");
            ViewBag.TimeSlotID = new SelectList(db.TimeSlots, "TimeSlotID", "Name");
            return View();
        }

        // POST: Appointments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        // Create for new customer.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CustomerID,AppointmentType,Date,TimeSlotID,Comments")] AppointmentCreateModel appointment)
        {
            if (ModelState.IsValid)
            {
                Appointment app = new Appointment
                {
                    CurrentState = Appointment.AppointmentState.Open,
                    AppointmentType = appointment.AppointmentType,
                    Date = appointment.Date,
                    TimeSlotID = appointment.TimeSlotID,
                    CustomerID = appointment.CustomerID,
                    Creator = manager.FindById(User.Identity.GetUserId()).Name,
                    Comments = new List<Comment>()
                };
                db.Appointments.Add(app);

                //change the state of customer from "hold" to "pending", if applicable.
                if (appointment.AppointmentType == Appointment.AppointmentReason.Install)
                {
                    db.Customers.Find(appointment.CustomerID).CustomerState = Customer.CustomerStatus.Pending;
                }
                db.SaveChanges();

                app.Comments.Add(new Comment
                {
                    AppointmentId = app.AppointmentID,
                    Creator = manager.FindById(User.Identity.GetUserId()).Name,
                    Value = appointment.Comments
                });

                //now see if we can get the field tech assigned.
                var customer = db.Customers.Find(app.CustomerID);
                var tower = db.Towers.Find(customer.TowerId);
                var fieldTech = db.FieldTechs.DefaultIfEmpty(null).FirstOrDefault(x=>x.InstallZoneID == tower.InstallZoneId);
                if (fieldTech != null)
                {
                    app.FieldTechID = fieldTech.FieldTechID;
                }

                db.SaveChanges();

                return RedirectToAction("View", new { id = app.AppointmentID });
            }

            ViewBag.CustomerID = new SelectList(db.Customers, "CustomerID", "Name", appointment.CustomerID);
            ViewBag.TimeSlotID = new SelectList(db.TimeSlots, "TimeSlotID", "Name", appointment.TimeSlotID);
            return View();
        }
        #endregion
        #region Delete - not really used.
        // GET: Appointments/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var appointment = db.Appointments.DefaultIfEmpty(null).FirstOrDefault(x => x.AppointmentID == id && (
                x.CurrentState == Appointment.AppointmentState.Cancelled ||
                x.CurrentState == Appointment.AppointmentState.Completed ||
                x.CurrentState == Appointment.AppointmentState.Failed));

            if (appointment == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            var app = new AppointmentCancelModel
            {
                AppointmentID = appointment.AppointmentID,
                CustomerID = appointment.CustomerID,
                Date = appointment.Date,
                Name = appointment.Customer.Name
            };

            return PartialView("_Delete", app);
        }

        // POST: Appointments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Appointment appointment = db.Appointments.Find(id);
            db.Appointments.Remove(appointment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion
        #region This is for processing orders - cancelling, rescheduling, completing.

        #region Cancel.
        public ActionResult Cancel(int? id)
        {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var appointment = db.Appointments.DefaultIfEmpty(null).FirstOrDefault(x => x.AppointmentID == id && (
                x.CurrentState != Appointment.AppointmentState.Cancelled ||
                x.CurrentState != Appointment.AppointmentState.Completed ||
                x.CurrentState != Appointment.AppointmentState.Failed));

            if (appointment == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            var app = new AppointmentCancelModel
            {
                AppointmentID = appointment.AppointmentID,
                CustomerID = appointment.CustomerID,
                Date = appointment.Date,
                Name = appointment.Customer.Name
            };
           

            if (appointment.AppointmentType == Appointment.AppointmentReason.Install)
            {
                //let the agent decide if customer gets held or cancelled, if it's an installation appointment.
                var CustomerStatusList = Enum.GetValues(typeof(Customer.CustomerStatus))

                .Cast<Customer.CustomerStatus>()
                .Where(e => e != Customer.CustomerStatus.Active && e != Customer.CustomerStatus.Pending)
                .Select(e => new SelectListItem
                {
                    Value = ((int)e).ToString(),
                    Text = e.ToString()
                });
                ViewBag.CustomerState = CustomerStatusList;

                return PartialView("_CancelInstall", app);
            }

            return PartialView("_Cancel", app);
        }

        //post to cancel.
        [HttpPost, ActionName("Cancel")]
        [ValidateAntiForgeryToken]
        public ActionResult CancelConfirmed(int? id, string Comments, int NextCustomerState = -1)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            Appointment appointment = db.Appointments.Find(id);
            appointment.Cancel(Comments, manager.FindById(User.Identity.GetUserId()).Name);
            if (NextCustomerState != -1)
            {
                Customer customer = db.Customers.Find(appointment.CustomerID);
                customer.Cancel((NextCustomerState == (int)Customer.CustomerStatus.Hold));
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Fail(int? id)
        {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var appointment = db.Appointments.DefaultIfEmpty(null).FirstOrDefault(x => x.AppointmentID == id && (
                x.CurrentState != Appointment.AppointmentState.Cancelled ||
                x.CurrentState != Appointment.AppointmentState.Completed ||
                x.CurrentState != Appointment.AppointmentState.Failed));

            if (appointment == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            var app = new AppointmentCancelModel
            {
                AppointmentID = appointment.AppointmentID,
                CustomerID = appointment.CustomerID,
                Date = appointment.Date,
                Name = appointment.Customer.Name
            };
           
            return PartialView("_Fail", app);
        }

        //post to fail - very similar to cancel, only one real difference.
        [HttpPost, ActionName("Fail")]
        [ValidateAntiForgeryToken]
        public ActionResult FailConfirmed(int? id, string Comments)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            Appointment appointment = db.Appointments.Find(id);
            appointment.Fail(Comments, manager.FindById(User.Identity.GetUserId()).Name);
            if (appointment.AppointmentType == Appointment.AppointmentReason.Install)
            {
                Customer customer = db.Customers.Find(appointment.CustomerID);
                customer.Cancel(false);
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Complete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var appointment = db.Appointments.DefaultIfEmpty(null).FirstOrDefault(x => x.AppointmentID == id && (
                x.CurrentState != Appointment.AppointmentState.Cancelled ||
                x.CurrentState != Appointment.AppointmentState.Completed ||
                x.CurrentState != Appointment.AppointmentState.Failed));

            if (appointment == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            var app = new AppointmentCancelModel
            {
                AppointmentID = appointment.AppointmentID,
                CustomerID = appointment.CustomerID,
                Date = appointment.Date,
                Name = appointment.Customer.Name
            };

            return PartialView("_Complete", app);
        }

        //post to fail - very similar to cancel, only one real difference.
        [HttpPost, ActionName("Complete")]
        [ValidateAntiForgeryToken]
        public ActionResult CompleteConfirmed(int? id, string Comments)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            Appointment appointment = db.Appointments.Find(id);
            appointment.Complete(Comments, manager.FindById(User.Identity.GetUserId()).Name);
            Customer customer = db.Customers.Find(appointment.CustomerID);
            customer.CustomerState = Customer.CustomerStatus.Active;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //toss in a comment.
        [HttpPost, ActionName("Comment")]
        public ActionResult AddComment(int id, string comment)
        {
            Appointment app = db.Appointments.Find(id);
            if (app == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = manager.FindById(User.Identity.GetUserId());

            app.AddComment(comment, user.Name);

            db.SaveChanges();

            return Json(new{state="Success!"});
        }

        public ActionResult GetComments(int id)
        {
            Appointment app = db.Appointments.Find(id);
            if (app == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return PartialView("_AppointmentsComments", app.Comments);
        }

        #endregion
        #region Reschedules.

        public ActionResult Reschedule(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Appointment appointment = db.Appointments.Find(id);
            if (appointment == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.TimeSlotID = new SelectList(db.TimeSlots, "TimeSlotID", "Name");
            return PartialView("_Reschedule", appointment);
        }

        //post to reschedule.
        [HttpPost, ActionName("Reschedule")]
        [ValidateAntiForgeryToken]
        public ActionResult Reschedule(int? id, DateTime Date, int TimeSlotID, string Comment )
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var app = db.Appointments.Find(id);
            app.Date = Date;
            app.TimeSlotID = TimeSlotID;
            app.AddComment(Comment, manager.FindById(User.Identity.GetUserId()).Name);

            db.SaveChanges();
            return RedirectToAction("View", new { id = id });
        }

        #endregion

       #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        #region check dates.
        [HttpGet]
        public ActionResult CheckDate(DateTime Date, string TimeSlot = "", string Location = "")
        {
            InstallZone location = null;
            TimeSlot timeSlot = null;
            //convert date to a readable string.
            ViewBag.Date = Date.ToString("yyyy-MM-dd");

            if (Location != "")
            {
                //check if there's any timeslot by that name.
                location = db.InstallZones.DefaultIfEmpty(null).First(x => x.Name.ToUpper() == Location.ToUpper());
                if (location == null)
                {
                    return RedirectToAction("Index");
                }
            }

            if (TimeSlot != "")
            {
                timeSlot = db.TimeSlots.DefaultIfEmpty(null).First(x => x.Name.ToUpper() == TimeSlot.ToUpper());
                if (timeSlot == null)
                {
                    return RedirectToAction("Index");
                }

            }

            //check if there is any special date limitation on this day.
            Schedule schedule = db.Schedules.DefaultIfEmpty(null).FirstOrDefault(x => x.Date == Date && x.DefaultOption == false);
            if (schedule == null)
            {
                schedule = db.Schedules.First(x => x.DefaultOption == true);
            }
            //if it can't find a schedule revert to index.
            if (schedule == null)
            {
                return RedirectToAction("Index");
            }

            //This sets the date so that the openings know what date they are on.
            schedule.Date = Date;

            //get the openings list.
            return View(schedule.ScheduleWithFilter(Date, timeSlot, location));
        }

        public ActionResult CheckDates(DateTime Date, string TimeSlot = "", string Location = "", int DateCount = 7)
        {
            InstallZone location = null;
            TimeSlot timeSlot = null;
            //convert date to a readable string.
            ViewBag.Date = Date.ToString("yyyy-MM-dd");

            if (Location != "")
            {
                //check if there's any timeslot by that name.
                location = db.InstallZones.DefaultIfEmpty(null).First(x => x.Name.ToUpper() == Location.ToUpper());
                if (location == null)
                {
                    return RedirectToAction("Index");
                }
            }

            if (TimeSlot != "")
            {
                timeSlot = db.TimeSlots.DefaultIfEmpty(null).First(x => x.Name.ToUpper() == TimeSlot.ToUpper());
                if (timeSlot == null)
                {
                    return RedirectToAction("Index");
                }

            }

            List<Schedule> schedules = new List<Schedule>();

            //check if there is any special date limitation on this day.
            for (int i = 0; i < Math.Min(DateCount, 1460); i++)
            {
                DateTime DateToCompare = Date.AddDays(i);
                Schedule schedule = db.Schedules.DefaultIfEmpty(null).FirstOrDefault(x => x.Date == DateToCompare && x.DefaultOption == false);

                if (schedule == null)
                {
                    schedule = db.Schedules.First(x => x.DefaultOption == true);
                }
                schedule = schedule.ScheduleWithFilter(DateToCompare, timeSlot, location);
                schedule.Date = DateToCompare;
                //schedule.Clone(schedule);
                Schedule scheduleToAdd = new Schedule();
                scheduleToAdd = schedule.Clone(scheduleToAdd);

                schedules.Add(scheduleToAdd);
            }

            //get the openings list.
            return View(schedules);
        }

        //merge this with above later.
        public ActionResult CheckDatesTable(DateTime Date, string TimeSlot = "", string Location = "", int DateCount = 7, bool FindStartOfWeek = false, int CustomerID = -1, int TowerID = -1)
        { 
            InstallZone location = null;
            TimeSlot timeSlot = null;
            //convert date to a readable string.
            ViewBag.Date = Date.ToString("yyyy-MM-dd");
            Date = new DateTime(Date.Year, Date.Month, Date.Day, 0, 0, 0, 0);

            //customerID is exclusive with Location.
            if (CustomerID != -1)
            {
                Location = db.Customers.First(x=>x.CustomerID == CustomerID).Tower.InstallZone.Name;
            }
            else if (TowerID != -1)
            {
                Location = db.Towers.First(x => x.TowerID == TowerID).InstallZone.Name;
            }
            if (Location != "")
            {
                //check if there's any timeslot by that name.
                location = db.InstallZones.DefaultIfEmpty(null).First(x => x.Name.ToUpper() == Location.ToUpper());
                if (location == null)
                {
                    return RedirectToAction("Index");
                }
            }

            if (TimeSlot != "")
            {
                timeSlot = db.TimeSlots.DefaultIfEmpty(null).First(x => x.Name.ToUpper() == TimeSlot.ToUpper());
                if (timeSlot == null)
                {
                    return RedirectToAction("Index");
                }

            }

            List<Schedule> schedules = new List<Schedule>();

            //check if there is any special date limitation on this day.
            for (int i = 0; i < Math.Min(DateCount, 1460); i++)
            {
                DateTime DateToCompare = Date.AddDays(i);
                Schedule schedule = db.Schedules.DefaultIfEmpty(null).FirstOrDefault(x => x.Date == DateToCompare && x.DefaultOption == false);

                if (schedule == null)
                {
                    schedule = db.Schedules.First(x => x.DefaultOption == true);
                }
                schedule = schedule.ScheduleWithFilter(DateToCompare, timeSlot, location);
                schedule.Date = DateToCompare;
                //schedule.Clone(schedule);
                Schedule scheduleToAdd = new Schedule();
                scheduleToAdd = schedule.Clone(scheduleToAdd);

                schedules.Add(scheduleToAdd);
            }

            if (Location != ""){
                ViewBag.InstallZones = db.InstallZones.OrderBy(x => x.Name).Where(x=>x.Name.ToUpper() == Location.ToUpper()).ToList();
            } else {
                ViewBag.InstallZones = db.InstallZones.OrderBy(x => x.Name).ToList();
            }

            if (TimeSlot != "")
            {
                ViewBag.TimeSlots = db.TimeSlots.OrderBy(x => x.Name).Where(x=>x.Name.ToUpper() == TimeSlot.ToUpper()).ToList();
            }
            else
            {
                ViewBag.TimeSlots = db.TimeSlots.OrderBy(x => x.Name).ToList();
            }

            //get the openings list.
            #region Send storage data, to allow scripting later.

            ViewBag.SettingsDate = Date;
            ViewBag.SettingsTimeSlot = TimeSlot;
            ViewBag.SettingsLocation = Location;
            ViewBag.SettingsDateCount = DateCount;
            ViewBag.SettingsFindStartOfWeek = FindStartOfWeek;
            ViewBag.SettingsCustomerID = CustomerID;
            ViewBag.SettingsTowerID = TowerID;

            #endregion
            return PartialView("_CheckDatesTable",schedules);
        }

        //this is to get appointments from tomorrow and onwards.
        public ActionResult GetUpcomingAppointments(int Amount)
        {
            //pick today's date.
            DateTime Now = DateTime.Now;
            List<Appointment> Appointments = db.Appointments.Where(x => x.Date > Now).OrderBy(x => x.Date).Take(Amount).ToList();

            return PartialView("_GetUpcomingAppointments", Appointments);
        }

        //changes the assigned tech. Admin only.
        [HttpPost]
        [Authorize(Roles="Admin")]
        public ActionResult UpdateAssignedTech(int id, int? tech)
        {
            Appointment app = db.Appointments.Find(id);
            if (app == null)
            {
                return Json(new { state = "Invalid Appointment ID" });
            }
            if (tech == null)
            {
                app.FieldTechID = null;
                return Json(new { state = "Success!" });
            }
            FieldTech fieldTech = db.FieldTechs.Find(tech);
            if (fieldTech == null || fieldTech.Active == false)
            {
                return Json(new { state = "Invalid Tech ID" });
            }

            app.FieldTechID = fieldTech.FieldTechID;
            db.SaveChanges();
            return Json (new {state="Success!"});
        }
        #endregion
    }
}
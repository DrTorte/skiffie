﻿using System;
using Skiffie.Models;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Skiffie.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [AllowAnonymous]
        public ActionResult NavBar()
        {
            List<NavigationBars> navs = new List<NavigationBars>();

            var identity = (ClaimsIdentity)User.Identity;
            var rank = identity.Claims.DefaultIfEmpty(null).FirstOrDefault(x => x.Type == ("UserRole"));
            if (rank != null)
            {
                if (rank.Value == "Field Tech" || rank.Value == "Agent" || rank.Value =="Admin")
                {
                    navs.AddRange(new List<NavigationBars>{
                        new NavigationBars{
                            Name="Appointments",
                            Action="Index",
                            Controller="Appointments"
                        },
                        new NavigationBars{
                            Name="Customers", 
                            Action="Index", 
                            Controller="Customers"
                        },
                        new NavigationBars{
                            Name="Towers", 
                            Action="Index", 
                            Controller="Towers"
                        },new NavigationBars{
                            Name="Suggestion Box",
                            Action="Index",
                            Controller="SuggestionBox"
                        },
                    });
                }
                if (rank.Value == "Admin")
                {
                    navs.AddRange(new List<NavigationBars>{
                        new NavigationBars{
                            Name="Schedules", 
                            Action="Index", 
                            Controller="Schedules",
                            Admin=true
                        },
                        new NavigationBars{
                            Name="Zones", 
                            Action="Index", 
                            Controller="Zones",
                            Admin=true
                        },
                        new NavigationBars{
                            Name="Time Slots", 
                            Action="Index", 
                            Controller="TimeSlots",
                            Admin=true
                        },
                        new NavigationBars{
                            Name="Manage Users",
                            Action="Index",
                            Controller="Account",
                            Admin=true
                        },
                        new NavigationBars{
                            Name="Field Techs",
                            Action="Index",
                            Controller="FieldTechs",
                            Admin=true
                        }
                    });
                }
            }
            return PartialView("_NavBar", navs);
        }

        //really just do this to get the agent's name :P
        [AllowAnonymous]
        public ActionResult LoginPartial()
        {
            List<NavigationBars> navs = new List<NavigationBars>();

            var identity = (ClaimsIdentity)User.Identity;
            var name = identity.Claims.DefaultIfEmpty(null).FirstOrDefault(x => x.Type == ("Name"));
            if (name != null)
            {
                ViewBag.Name = name.Value;
            }

            return PartialView("_LoginPartial");
        }
    }
}
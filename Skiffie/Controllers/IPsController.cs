﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Skiffie.Models;

namespace Skiffie.Controllers
{
    [Authorize]
    public class IPsController : Controller
    {
        private SkiffieContext db = new SkiffieContext();

        // GET: IPs
        public ActionResult Index()
        {
            return View(db.IPs.ToList());
        }

        // GET: IPs/Details/5
        [Authorize(Roles = "Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IP iP = db.IPs.Find(id);
            if (iP == null)
            {
                return HttpNotFound();
            }
            return View(iP);
        }

        // GET: IPs/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerID", "Name");
            return View();
        }

        // POST: IPs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Create([Bind(Include = "IPId,Subnet,IPAddress,Available,CustomerId")] IP iP)
        {
            if (ModelState.IsValid)
            {
                db.IPs.Add(iP);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(iP);
        }

        // GET: IPs/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IP iP = db.IPs.Find(id);
            if (iP == null)
            {
                return HttpNotFound();
            }
            return View(iP);
        }

        // POST: IPs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit([Bind(Include = "IPId,Subnet,IPAddress,Available,CustomerId")] IP iP)
        {
            if (ModelState.IsValid)
            {
                db.Entry(iP).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(iP);
        }

        // GET: IPs/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IP iP = db.IPs.Find(id);
            if (iP == null)
            {
                return HttpNotFound();
            }
            return View(iP);
        }

        // POST: IPs/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            IP iP = db.IPs.Find(id);
            db.IPs.Remove(iP);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //block new IPs.
        [Authorize(Roles = "Admin")]
        public ActionResult IPBlock(int TowerEditID, int StartIP, int EndIP, string Comments = "")
        {
            if (StartIP < 1 || EndIP < 1 || StartIP > 255 || EndIP > 255)
            {
                return Json(new { error = "Values not in range" });
            }
            var IPs = db.IPs.Where(x => x.TowerId == TowerEditID);
            if (IPs == null || IPs.Count() == 0)
            {
                return Json(new {error= "No IPs found for this tower!"});
            }
            for (byte i = (byte)StartIP; i <= (byte)EndIP; i++)
            {
                IPs.First(x => x.IPAddress == i).CustomerUseable = false;
                IPs.First(x => x.IPAddress == i).Comment = Comments;
            }
            db.SaveChanges();
            return Json(new { result = "IPs blocked." });
        }

        //block new IPs.
        [Authorize(Roles = "Admin")]
        public ActionResult IPPermit(int TowerEditID, int StartIP, int EndIP, string Comments = "")
        {
            if (StartIP < 1 || EndIP < 1 || StartIP > 255 || EndIP > 255)
            {
                return Json(new { error = "Values not in range" });
            }
            var IPs = db.IPs.Where(x => x.TowerId == TowerEditID);
            if (IPs == null || IPs.Count() == 0)
            {
                return Json(new { error = "No IPs found for this tower!" });
            }
            for (byte i = (byte)StartIP; i <= (byte)EndIP; i++)
            {
                IPs.First(x => x.IPAddress == i).CustomerUseable = true;
                IPs.First(x => x.IPAddress == i).Comment = Comments;
            }
            db.SaveChanges();
            return Json(new { result = "IPs permitted." });
        }

        //get all the currently blocked IPs in a partial view list.
        public ActionResult GetBlockedIPs(int TowerEditID)
        {
            var IPs = db.IPs.Where(x => x.TowerId == TowerEditID && x.CustomerUseable == false);

            return PartialView("_BlockedIPs", IPs);
        }
    }
}

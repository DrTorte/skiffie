﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Skiffie.Models;

namespace Skiffie.Controllers
{
    [Authorize]
    public class SchedulesController : Controller
    {
        private SkiffieContext db = new SkiffieContext();

        // GET: Schedules
        public ActionResult Index()
        {
            return View(db.Schedules.OrderByDescending(x=>x.DefaultOption).ThenBy(x=>x.Date).ToList());
        }

        public ActionResult SingleDay(){
            ViewBag.InstallZones = db.InstallZones.OrderBy(x => x.Name);
            ViewBag.TimeSlots = db.TimeSlots.OrderBy(x => x.Name);

            return PartialView("SingleDaySchedule", new Openings());
        }

        public ActionResult Week(){
            ViewBag.InstallZones = db.InstallZones.OrderBy(x => x.Name);
            ViewBag.TimeSlots = db.TimeSlots.OrderBy(x => x.Name);

            return PartialView("WeekSchedule", new Openings());
        }

        // GET: Schedules/Details/5
        [Authorize(Roles = "Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Schedule schedule = db.Schedules.Find(id);
            if (schedule == null)
            {
                return HttpNotFound();
            }
            return View(schedule);
        }

        // GET: Schedules/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.TimeSlots = db.TimeSlots.OrderBy(x => x.TimeSlotID);
            ViewBag.Zones = db.InstallZones.OrderBy(x => x.Name);
            Schedule model = new Schedule();
            model.Date = DateTime.Today;
            return View("Create", model);
        }

        // POST: Schedules/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Create([Bind(Include = "ScheduleId,DefaultOption,Date")] Schedule schedule, List<int> Day, List<int> InstallZoneId, List<int> TimeSlotId, List<int> Amount)
        { 
            HttpContext.Request.InputStream.Position = 0;
            var result = new System.IO.StreamReader(HttpContext.Request.InputStream).ReadToEnd();

            //specify how many values we should see if it's a specific date.
            int specificDayCount = db.InstallZones.Count() * db.TimeSlots.Count();

            //Specify how many vlaues we should see if it's a full week.
            int weekCount = specificDayCount*7;

            //prepare everything in a nice class list.
            List<ScheduledDate> schedules = CreateDate(Day, InstallZoneId, TimeSlotId, Amount);

            if (ModelState.IsValid && schedules != null)
            {
                bool issueFound = false;
                //now do a check on the date count.
                if (schedule.DefaultOption == false)
                {
                    List<ScheduledDate> daysToWork = schedules.Where(x => x.Day == -1).ToList();
                    //set the days to add 
                    //check if the amount of days are right.
                    if (daysToWork.Count == specificDayCount)
                    {
                        schedule.Openings = new List<Openings>();
                        foreach (ScheduledDate x in daysToWork)
                        {
                            schedule.Openings.Add(new Openings
                            {
                                Schedule = schedule,
                                Amount = x.Amount,
                                Day = x.Day,
                                InstallZoneId = x.Zone,
                                InstallZone = db.InstallZones.First(y => y.InstallZoneId == x.Zone),
                                TimeSlotId = x.TimeSlot,
                                TimeSlot = db.TimeSlots.First(y => y.TimeSlotID == x.TimeSlot)
                            });
                        }
                    }
                    else
                    {
                        issueFound = true;
                    }
                }
                else
                {
                    List<ScheduledDate> daysToWork = schedules.Where(x => x.Day > -1).ToList();
                    //check if the days are right.
                    if (daysToWork.Count == weekCount)
                    {
                        schedule.Openings = new List<Openings>();
                        //make sure only one default state is active at any time.
                        foreach (var z in db.Schedules.Where(x => x.DefaultOption == true).ToList())
                        {
                            foreach (var y in z.Openings.ToList())
                            {
                                db.Openings.Remove(y);
                            }
                            db.Schedules.Remove(z);
                        }
                        foreach (ScheduledDate x in daysToWork)
                        {
                            schedule.Openings.Add(new Openings
                            {
                                Schedule = schedule,
                                Amount = x.Amount,
                                Day = x.Day,
                                InstallZoneId = x.Zone,
                                InstallZone = db.InstallZones.First(y => y.InstallZoneId == x.Zone),
                                TimeSlotId = x.TimeSlot,
                                TimeSlot = db.TimeSlots.First(y => y.TimeSlotID == x.TimeSlot)
                            });
                        }
                    }
                    else
                    {
                        issueFound = true;
                    }
                }
                if (issueFound) {
                    return View(schedule);
                }
                schedule.Active = true;
                db.Schedules.Add(schedule);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TimeSlots = db.TimeSlots.OrderBy(x => x.TimeSlotID);
            ViewBag.Zones = db.InstallZones.OrderBy(x => x.Name);

            return View(schedule);
        }

        // GET: Schedules/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Schedule schedule = db.Schedules.Find(id);
            if (schedule == null)
            {
                return HttpNotFound();
            }
            return View(schedule);
        }

        // POST: Schedules/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Schedule schedule = db.Schedules.Find(id);

            foreach (var opening in schedule.Openings.ToList())
            {
                db.Openings.Remove(opening);
            }

            db.Schedules.Remove(schedule);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private class ScheduledDate {
            public int Day { get; set; }
            public int TimeSlot { get; set; }
            public int Zone { get; set; }
            public int Amount { get; set; }
        }

        private List<ScheduledDate> CreateDate(List<int> Days, List<int> Zones, List<int> Times, List<int> Amounts) {
            if (Days.Count == Zones.Count && Zones.Count == Times.Count && Times.Count == Amounts.Count)
            {
                List<ScheduledDate> returnValue = new List<ScheduledDate>();
                for (int i = 0; i < Days.Count; i++)
                {
                    returnValue.Add(new ScheduledDate
                    {
                        Day = Days[i],
                        TimeSlot = Times[i],
                        Zone = Zones[i],
                        Amount = Amounts[i]
                    });
                }
                return returnValue;
            }
            else
            {
                return null;
            }
        }
    }
}

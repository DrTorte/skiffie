﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Skiffie.Models;

namespace Skiffie.Controllers
{
    [Authorize]
    public class TowersController : Controller
    {
        private SkiffieContext db = new SkiffieContext();

        // GET: Towers
        public ActionResult Index()
        {
            var towers = db.Towers.Include(t => t.InstallZone).OrderBy(x=>x.InstallZone.Name).ThenBy(x=>x.TowerName);

            if (User.IsInRole("Admin")){
                return View("IndexAdmin", towers.ToList());
            }
            return View(towers.ToList());
        }

        // GET: Towers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tower tower = db.Towers.Find(id);
            if (tower == null)
            {
                return HttpNotFound();
            }

            //check user role.

            return View(tower);
        }

        // GET: Towers/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.InstallZoneId = new SelectList(db.InstallZones, "InstallZoneId", "Name");
            return View();
        }

        // POST: Towers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TowerName,Subnet,InstallZoneId")] TowerCreate towerCreate, 
            List<string> APName, List<string> APComment, List<AccessPoint.AccessPointState> APState)
        {
            if (db.IPs.Count(x => x.Subnet == towerCreate.Subnet) > 0)
            {
                //Subnet already taken.
                ViewBag.InstallZoneId = new SelectList(db.InstallZones, "InstallZoneId", "Name");
                ViewBag.SubnetError = "This subnet was taken.";
                return View(towerCreate);
            }
            if (ModelState.IsValid)
            {
                //create the actual tower item.
                Tower tower = new Tower();
                tower.TowerName = towerCreate.TowerName;
                tower.Subnet = towerCreate.Subnet;
                tower.InstallZoneId = towerCreate.InstallZoneId;

                //create the lists and then add them.
                tower.APs = new List<AccessPoint>();
                tower.IPs = new List<IP>();

                for (int i = 0; i < tower.APsToCreate; i++)
                {
                    tower.APs.Add(new AccessPoint
                    {
                        Name = tower.TowerName + (i + 1).ToString(),
                        CurrentState = AccessPoint.AccessPointState.Open
                    });
                }

                for (int i = 1; i < 256; i++)
                {
                    tower.IPs.Add(new IP
                    {
                        Subnet = tower.Subnet,
                        IPAddress = (byte) i,
                        CustomerUseable = true
                    });
                }

                db.Towers.Add(tower);
                db.SaveChanges();

                for (int i = 0; i < APName.Count(); i++)
                {
                    tower.APs.Add(new AccessPoint
                    {
                        Name = APName[i],
                        Comment = APName[i],
                        CurrentState = (AccessPoint.AccessPointState) APState[i],
                        TowerId = tower.TowerID
                    });
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.InstallZoneId = new SelectList(db.InstallZones, "InstallZoneId", "Name", towerCreate.InstallZoneId);
            return View(towerCreate);
        }

        // GET: Towers/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tower tower = db.Towers.Find(id);
            if (tower == null)
            {
                return HttpNotFound();
            
            }

            ViewBag.InstallZoneId = new SelectList(db.InstallZones, "InstallZoneId", "Name", tower.InstallZoneId);
            TowerEdit towerEdit = new TowerEdit
            {
                TowerName = tower.TowerName,
                TowerEditID = tower.TowerID,
                InstallZoneId = tower.InstallZoneId,
                Subnet = tower.Subnet,
                APs = tower.APs
            };
            return View(towerEdit);
        }

        // POST: Towers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit([Bind(Include = "TowerEditID,TowerName,InstallZoneId")] TowerEdit towerEdit,
            List<string> APName, List<string> APComment, List<AccessPoint.AccessPointState> APState,
            [Bind(Prefix="APs")]List<AccessPoint> AP)
        {
            if (ModelState.IsValid)
            {
                Tower tower = db.Towers.Find(towerEdit.TowerEditID);
                if (tower == null)
                {
                    ViewBag.InstallZoneId = new SelectList(db.InstallZones, "InstallZoneId", "Name", towerEdit.InstallZoneId);
                    return View(tower);
                }
                if (AP != null && AP.Count > 0)
                {
                    foreach (var ap in AP)
                    {
                        var myAP = db.AccessPoints.Find(ap.AccessPointID);
                        myAP.Comment = ap.Comment;
                        myAP.CurrentState = ap.CurrentState;
                        myAP.Name = ap.Name;
                    }
                }

                tower.TowerName = towerEdit.TowerName;
                tower.InstallZoneId = towerEdit.InstallZoneId;

                if (APName != null)
                {
                    for (int i = 0; i < APName.Count(); i++)
                    {
                        tower.APs.Add(new AccessPoint
                        {
                            Name = APName[i],
                            Comment = APName[i],
                            CurrentState = (AccessPoint.AccessPointState)APState[i],
                            TowerId = tower.TowerID
                        });
                    }
                }

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.InstallZoneId = new SelectList(db.InstallZones, "InstallZoneId", "Name", towerEdit.InstallZoneId);
            return View(towerEdit);
        }

        //get and set up the APs.
        public ActionResult GetAPsFromTower(int id)
        {
            var APs = db.Towers.Find(id).APs;
            List<APCreate> aps = new List<APCreate>();

            APs.ForEach(x=> aps.Add(new APCreate{
                APComment = x.Comment,
                APCreateID = x.AccessPointID,
                APName = x.Name,
                APState = x.CurrentState
            }));

            return PartialView("_APEdit", aps);
        }

        // GET: Towers/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tower tower = db.Towers.Find(id);
            if (tower == null)
            {
                return HttpNotFound();
            }
            return View(tower);
        }

        // POST: Towers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Tower tower = db.Towers.Find(id);
            foreach (var ap in tower.APs.ToList())
            {
                db.AccessPoints.Remove(ap);
            }
            foreach (var ip in tower.IPs.ToList())
            {
                db.IPs.Remove(ip);
            }
            db.Towers.Remove(tower);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpGet]
        public ActionResult GetTowers()
        {
            var Towers = db.Towers.OrderBy(x => x.InstallZone.Name).ThenBy(x=>x.TowerName);

            return PartialView("_TowerTable", Towers);
        }
        
        //update APs.
        [Authorize(Roles="Admin")]
        public string UpdateAPState(int id, int value)
        {
            AccessPoint AP = db.AccessPoints.Find(id);
            AP.CurrentState = (AccessPoint.AccessPointState) value;
            db.SaveChanges();
            return "Success!";
        }

        [HttpGet]
        public ActionResult APCreate()
        {
            return PartialView("_APCreate");
        }

        //removes current AP.
        [Authorize(Roles="Admin")]
        public string RemoveExistingAP(int id)
        {
            AccessPoint APToRemove = db.AccessPoints.Find(id);
            if (APToRemove == null)
            {
                return "Fail!";
            }
            db.AccessPoints.Remove(APToRemove);
            db.SaveChanges();
            return "Success!";
        }
    }
}

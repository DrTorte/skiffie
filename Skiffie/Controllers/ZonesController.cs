﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Skiffie.Models;

namespace Skiffie.Controllers
{
    [Authorize]
    public class ZonesController : Controller
    {
        private SkiffieContext db = new SkiffieContext();

        // GET: Zones
        public ActionResult Index()
        {
            return View(db.InstallZones.ToList());
        }

        // GET: Zones/Details/5
        [Authorize(Roles = "Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InstallZone installZone = db.InstallZones.Find(id);
            if (installZone == null)
            {
                return HttpNotFound();
            }
            return View(installZone);
        }

        // GET: Zones/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Zones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Create([Bind(Include = "InstallZoneId,Name")] InstallZone installZone)
        {
            if (ModelState.IsValid)
            {
                db.InstallZones.Add(installZone);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(installZone);
        }

        // GET: Zones/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InstallZone installZone = db.InstallZones.Find(id);
            if (installZone == null)
            {
                return HttpNotFound();
            }
            return View(installZone);
        }

        // POST: Zones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit([Bind(Include = "InstallZoneId,Name")] InstallZone installZone)
        {
            if (ModelState.IsValid)
            {
                db.Entry(installZone).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(installZone);
        }

        // GET: Zones/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InstallZone installZone = db.InstallZones.Find(id);
            if (installZone == null)
            {
                return HttpNotFound();
            }
            return View(installZone);
        }

        // POST: Zones/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            InstallZone installZone = db.InstallZones.Find(id);
            db.InstallZones.Remove(installZone);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Data.Entity;
using Skiffie.Models;
using Skiffie.Migrations;

namespace Skiffie
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<SkiffieContext, Configuration>());
            SkiffieContext db = new SkiffieContext();
            if (db.Users.Count() == 0)
            {
                SkiffieDbInitializer init = new SkiffieDbInitializer();
                init.TrySeed(db);
            }

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}

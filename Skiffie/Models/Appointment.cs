﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Skiffie.Models
{
    public class Appointment
    {
        public enum AppointmentState { Open, PendingCompletion, PendingAssignment, PendingDispatch, Dispatched, Failed, Completed, NeedsReschedule, Cancelled }
        public enum AppointmentReason { Install, Repair, Misc }

        public virtual int AppointmentID { get; set; }

        public virtual int CustomerID { get; set; }
        public virtual Customer Customer { get; set; }

        [Required]
        public virtual AppointmentReason AppointmentType { get; set; }

        [DataType(DataType.Date)]
        public virtual DateTime Date { get; set; }

        [Display(Name="Time Slot")]
        public virtual int? TimeSlotID { get; set; }
        public virtual TimeSlot TimeSlot { get; set; }

        public virtual string Creator { get; set; }

        public virtual int? FieldTechID { get; set; }
        public virtual FieldTech FieldTech { get; set; }

        public virtual AppointmentState CurrentState { get; set; }

        public virtual List<Comment> Comments { get; set; }

        public void Cancel(string Comment, string Creator)
        {
            AddComment(Comment, Creator);
            CurrentState = AppointmentState.Cancelled;
        }

        public void Complete(string Comment, string Creator)
        {
            AddComment(Comment, Creator);
            CurrentState = AppointmentState.Completed;
        }

        public void Fail(string Comment, string Creator)
        {
            AddComment(Comment, Creator);
            CurrentState = AppointmentState.Failed;
        }

        public void AddComment(string Comment, string Creator)
        {
            Comments.Add(new Comment
            {
                AppointmentId = AppointmentID,
                Creator = Creator,
                Value = Comment
            });
        }

    }

    public class AppointmentTableView
    {
        public virtual int AppointmentID { get; set; }

        public virtual int CustomerID { get; set; }
        public virtual Customer Customer { get; set; }

        [Required]
        public virtual Skiffie.Models.Appointment.AppointmentReason AppointmentType { get; set; }

        [DataType(DataType.Date)]
        public virtual DateTime Date { get; set; }

        [Display(Name = "Time Slot")]
        public virtual int? TimeSlotID { get; set; }
        public virtual TimeSlot TimeSlot { get; set; }

        public virtual int? FieldTechID { get; set; }
        public virtual FieldTech FieldTech { get; set; }
        public virtual Skiffie.Models.Appointment.AppointmentState CurrentState { get; set; }

        public string Classes { get; set; }

        public AppointmentTableView(Appointment app)
        {
            AppointmentID = app.AppointmentID;
            Customer = app.Customer;
            CustomerID = app.CustomerID;
            AppointmentType = app.AppointmentType;
            Date = app.Date;
            TimeSlot = app.TimeSlot;
            TimeSlotID = app.TimeSlotID;
            FieldTechID = app.FieldTechID;
            if (app.FieldTech != null)
            {
                FieldTech = app.FieldTech;
            }
            CurrentState = app.CurrentState;
        }
    }

    public class AppointmentCreateModel
    {
        [Display(Name="Type of Appointment")]
        [Required]
        public Appointment.AppointmentReason AppointmentType { get; set; }

        public int CustomerID { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public virtual DateTime Date { get; set; }

        [Display(Name = "Time Slot")]
        public virtual int? TimeSlotID { get; set; }
        public virtual TimeSlot TimeSlot { get; set; }

        public virtual string Comments { get; set; }
    }

    public class AppointmentCancelModel
    {
        public int id { get; set; }
        public int AppointmentID { get; set; }

        public int CustomerID { get; set; }

        [Display(Name = "Customer")]
        public string Name { get; set; }

        [Display(Name = "Customer's next state:")]
        public Customer.CustomerStatus NewCustomerState { get; set; }

        public virtual DateTime Date { get; set; }

        [Required]
        [MinLength(3)]
        public virtual string Comments { get; set; }
    }

    public class AppointmentViewModel
    {
        public int AppointmentViewModelId { get; set; }

        public int CustomerID { get; set; }
        
        public Appointment.AppointmentReason AppointmentType { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        public virtual int? TimeSlotID { get; set; }
        public virtual TimeSlot TimeSlot { get; set; }

        public virtual string Creator { get; set; }

        public virtual Appointment.AppointmentState CurrentState { get; set; }

        public virtual List<Comment> Comments { get; set; }

        public string Comment { get; set; }

        public int? TechsId { get; set; }
        public IEnumerable<SelectListItem> Techs { get; set; }
        //constructor.
        public AppointmentViewModel(Appointment app) {
            AppointmentViewModelId = app.AppointmentID;
            CustomerID = app.CustomerID;
            AppointmentType = app.AppointmentType;
            Date = app.Date;
            TimeSlotID = app.TimeSlotID;
            TimeSlot = app.TimeSlot;
            Creator = app.Creator;
            CurrentState = app.CurrentState;
            Comments = app.Comments;
        }
    }
}
﻿using System;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;

namespace Skiffie.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public int SkiffieRoleId { get; set; }
        public string Name { get; set; }
        public List<AccessClaims> MyClaims { get;set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            SkiffieContext db = new SkiffieContext();
            userIdentity.AddClaim(new Claim("UserRole", db.SkiffieRoles.FirstAsync(x=>x.SkiffieRolesId == SkiffieRoleId).Result.Name));

            MyClaims = db.SkiffieRoles.FirstAsync(x => x.SkiffieRolesId == SkiffieRoleId).Result.AccessClaims;

            MyClaims.ForEach(d=>
                userIdentity.AddClaim(new Claim(d.ClaimName, d.ClaimValue))
            );

            userIdentity.AddClaim(new Claim("Name", Name == null ? Email : Name));
            userIdentity.AddClaim(new Claim("Email", Email));
            

            return userIdentity;
        }
    }
}
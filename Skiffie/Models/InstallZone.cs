﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skiffie.Models
{
    public class InstallZone
    {
        public virtual int InstallZoneId { get; set; }
        public virtual string Name { get; set; }
    }
}

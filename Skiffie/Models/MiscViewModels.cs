﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Skiffie.Models
{
    public class NavigationBars
    {
        public String Name { get; set; }
        public String Controller { get; set; }
        public String Action { get; set; }
        public bool? Admin { get; set; }
    }

    public class Comment
    {
        public virtual int CommentId { get; set; }

        public virtual int AppointmentId { get; set; }
        public string Creator { get; set; }
        public string Value { get; set; }
    }
}
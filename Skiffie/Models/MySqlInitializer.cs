﻿using Skiffie.Models;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web.Configuration;

namespace Skiffie
{
    public class MySqlInitializer : IDatabaseInitializer<SkiffieContext>
    {
        public void InitializeDatabase(SkiffieContext context)
        {
            if (WebConfigurationManager.AppSettings["MySQL"] == "true")
            {
                if (!context.Database.Exists())
                {
                    // if database did not exist before - create it
                    context.Database.Create();
                }
                else
                {
                    // query to check if MigrationHistory table is present in the database 
                    var migrationHistoryTableExists = ((IObjectContextAdapter)context).ObjectContext.ExecuteStoreQuery<int>(
                    string.Format(
                      "SELECT MigrationId from _MigrationHistory"));

                    // if MigrationHistory table is not there (which is the case first time we run) - create it
                    if (migrationHistoryTableExists.FirstOrDefault() == 0)
                    {
                        context.Database.Delete();
                        context.Database.Create();
                        if (context.Users.Count() == 0)
                        {
                            var myInitializer = new SkiffieDbInitializer();
                        }
                    }
                }
            }
        }
    }
}
﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Skiffie.Models
{
    public class SkiffieContext : IdentityDbContext<ApplicationUser>
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx

        public SkiffieContext()
            : base("name=SkiffieContext")
        {
        }

        public static SkiffieContext Create()
        {
            return new SkiffieContext();
        }
        
        public System.Data.Entity.DbSet<Skiffie.Models.AccessPoint> AccessPoints { get; set; }

        public System.Data.Entity.DbSet<Skiffie.Models.IP> IPs { get; set; }

        public System.Data.Entity.DbSet<Skiffie.Models.Tower> Towers { get; set; }

        public System.Data.Entity.DbSet<Skiffie.Models.TimeSlot> TimeSlots { get; set; }

        public System.Data.Entity.DbSet<Skiffie.Models.InstallZone> InstallZones { get; set; }

        public System.Data.Entity.DbSet<Skiffie.Models.Appointment> Appointments { get; set; }

        public System.Data.Entity.DbSet<Skiffie.Models.Openings> Openings { get; set; }

        public System.Data.Entity.DbSet<Skiffie.Models.Schedule> Schedules { get; set; }

        public System.Data.Entity.DbSet<Skiffie.Models.Customer> Customers { get; set; }

        public System.Data.Entity.DbSet<Skiffie.Models.AccessClaims> AccessClaims { get; set; }

        public System.Data.Entity.DbSet<Skiffie.Models.SkiffieRoles> SkiffieRoles { get; set; }
        
        public System.Data.Entity.DbSet<Skiffie.Models.APCreate> APCreates { get; set; }

        public System.Data.Entity.DbSet<Skiffie.Models.Comment> Comments { get; set; }

        public System.Data.Entity.DbSet<Skiffie.Models.SuggestionBox> Suggestions { get; set; }

        public System.Data.Entity.DbSet<Skiffie.Models.SuggestionComment> SuggestionComments { get; set; }

        public System.Data.Entity.DbSet<Skiffie.Models.FieldTech> FieldTechs { get; set; }
    }
}

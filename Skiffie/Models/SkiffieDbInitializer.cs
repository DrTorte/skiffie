﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Skiffie.Models 
{
    public class SkiffieDbInitializer : System.Data.Entity.CreateDatabaseIfNotExists<SkiffieContext>
    {
        public void TrySeed(SkiffieContext Context)
        {
            Seed(Context);
        }

        protected override void Seed (SkiffieContext Context)
        {

            if (Context.Users.Count() == 0)
            {
                #region Install Zones, Time Slots, Schedule, claim types.
                #region Claims!
                var claims = new List<AccessClaims>{
                new AccessClaims{
                    ClaimName="Appointments",
                    ClaimValue="Read",
                    IncludedClaim=true
                },
                new AccessClaims{
                    ClaimName="Appointments",
                    ClaimValue="Write",
                    IncludedClaim=true
                },
                new AccessClaims{
                    ClaimName="Appointments",
                    ClaimValue="Process",
                    IncludedClaim=true
                },
                new AccessClaims{
                    ClaimName="Appointments",
                    ClaimValue="Override",
                    IncludedClaim=true
                },
                new AccessClaims{
                    ClaimName="TimeSlots",
                    ClaimValue="Read",
                    IncludedClaim=true
                },
                new AccessClaims{
                    ClaimName="TimeSlots",
                    ClaimValue="Write",
                    IncludedClaim=true
                },
                new AccessClaims{
                    ClaimName="InstallZones",
                    ClaimValue="Read",
                    IncludedClaim=true
                },
                new AccessClaims{
                    ClaimName="InstallZones",
                    ClaimValue="Write",
                    IncludedClaim=true
                },
                new AccessClaims{
                    ClaimName="Towers",
                    ClaimValue="Read",
                    IncludedClaim=true
                },
                new AccessClaims{
                    ClaimName="Towers",
                    ClaimValue="Write",
                    IncludedClaim=true
                },
                new AccessClaims{
                    ClaimName="AccessPoints",
                    ClaimValue="Read",
                    IncludedClaim=true
                },
                new AccessClaims{
                    ClaimName="AccessPoints",
                    ClaimValue="Write",
                    IncludedClaim=true
                },
                new AccessClaims{
                    ClaimName="Customer",
                    ClaimValue="Read",
                    IncludedClaim=true
                },
                new AccessClaims{
                    ClaimName="Customer",
                    ClaimValue="Write",
                    IncludedClaim=true
                },
                new AccessClaims{
                    ClaimName="Schedule",
                    ClaimValue="Read",
                    IncludedClaim=true
                },
                new AccessClaims{
                    ClaimName="Schedule",
                    ClaimValue="Write",
                    IncludedClaim=true
                },
                new AccessClaims{
                    ClaimName="Accounts",
                    ClaimValue="Read",
                    IncludedClaim=true
                },
                new AccessClaims{
                    ClaimName="Accounts",
                    ClaimValue="Write",
                    IncludedClaim=true
                }
            };
                #endregion

                claims.ForEach(d => Context.AccessClaims.Add(d));

                var installZones = new List<InstallZone> {
                
                new InstallZone { Name="East"},
                new InstallZone {Name ="West"}
            };

                installZones.ForEach(d => Context.InstallZones.Add(d));

                var timeSlots = new List<TimeSlot> {
                new TimeSlot {Name="AM"},
                new TimeSlot {Name="PM"}
            };

                timeSlots.ForEach(d => Context.TimeSlots.Add(d));

                var schedule = new Schedule
                {
                    DefaultOption = true,
                    Active = true,
                    Date = DateTime.Today
                };

                Context.Schedules.Add(schedule);

                Context.SaveChanges();
                #endregion
                #region Towers and Roles.

                //define field tech claims here.
                var fieldTechClaims = new List<AccessClaims>();

                fieldTechClaims.AddRange(claims.Where(x => x.ClaimValue == "Read"));
                fieldTechClaims.AddRange(claims.Where(x => x.ClaimName == "Appointments" && (x.ClaimValue == "Process" || x.ClaimValue == "Write")));

                var agentClaims = new List<AccessClaims>();
                agentClaims.AddRange(claims.Where(x => x.ClaimValue == "Read"));
                agentClaims.AddRange(claims.Where(x => x.ClaimName == "Appointments" && x.ClaimName == "Write"));

                var roles = new List<SkiffieRoles> {
                new SkiffieRoles{
                    Name = "Admin",
                    IncludedRole=true,
                    AccessClaims = claims
                },
                new SkiffieRoles{
                    Name="Field Tech",
                    IncludedRole=true,
                    AccessClaims = fieldTechClaims
                }, 
                new SkiffieRoles{
                    Name="Agent",
                    IncludedRole=true,
                    AccessClaims = agentClaims
                }
            };

                roles.ForEach(d => Context.SkiffieRoles.Add(d));

                var openings = new List<Openings>();
                foreach (var z in installZones)
                {
                    foreach (var t in timeSlots)
                    {
                        for (int i = 0; i < 7; i++)
                        {
                            int amount = i == 0 ? 0 : 4;
                            openings.Add(new Openings
                            {
                                Amount = amount,
                                Day = i,
                                ScheduleID = schedule.ScheduleId,
                                InstallZoneId = z.InstallZoneId,
                                TimeSlotId = t.TimeSlotID
                            });
                        }
                    }
                }

                openings.ForEach(d => Context.Openings.Add(d));

                var towers = new List<Tower> { 
                new Tower{ TowerName="Chatham", Subnet="10.1.1", APsToCreate = 6, InstallZoneId = installZones[0].InstallZoneId},
                new Tower{ TowerName="Essex", Subnet="10.1.2", APsToCreate = 3, InstallZoneId = installZones[1].InstallZoneId},
            };
                towers.ForEach(d => Context.Towers.Add(d));

                Context.SaveChanges();
                #endregion

                #region APs and IPs.


                List<IP> IPs = new List<IP>();

                for (byte i = 2; i < 255; i++)
                {
                    IPs.Add(new IP
                    {
                        Subnet = "10.1.1",
                        IPAddress = i,
                        TowerId = towers[0].TowerID
                    });
                }

                for (byte i = 2; i < 255; i++)
                {
                    IPs.Add(new IP
                    {
                        Subnet = "10.1.2",
                        IPAddress = i,
                        TowerId = towers[1].TowerID
                    });
                }
                IPs.ForEach(d => Context.IPs.Add(d));

                var APs = new List<AccessPoint>{
                new AccessPoint{Name="Chatham 1", TowerId = towers[0].TowerID, CurrentState = AccessPoint.AccessPointState.Open},
                new AccessPoint{Name="Chatham 2", TowerId = towers[0].TowerID, CurrentState = AccessPoint.AccessPointState.Open},
                new AccessPoint{Name="Chatham 3", TowerId = towers[0].TowerID, CurrentState = AccessPoint.AccessPointState.Open},
                new AccessPoint{Name="Chatham 4", TowerId = towers[0].TowerID, CurrentState = AccessPoint.AccessPointState.Open},
                new AccessPoint{Name="Chatham 5", TowerId = towers[0].TowerID, CurrentState = AccessPoint.AccessPointState.Open},
                new AccessPoint{Name="Chatham 6", TowerId = towers[0].TowerID, CurrentState = AccessPoint.AccessPointState.Open},

                new AccessPoint{Name="Essex 1", TowerId = towers[1].TowerID, CurrentState = AccessPoint.AccessPointState.Open},
                new AccessPoint{Name="Essex 2", TowerId = towers[1].TowerID, CurrentState = AccessPoint.AccessPointState.Open},
                new AccessPoint{Name="Essex 3", TowerId = towers[1].TowerID, CurrentState = AccessPoint.AccessPointState.Open},
                new AccessPoint{Name="Essex 4", TowerId = towers[1].TowerID, CurrentState = AccessPoint.AccessPointState.Open},
                new AccessPoint{Name="Essex 5", TowerId = towers[1].TowerID, CurrentState = AccessPoint.AccessPointState.Open},
                new AccessPoint{Name="Essex 6", TowerId = towers[1].TowerID, CurrentState = AccessPoint.AccessPointState.Open}
            };

                APs.ForEach(d => Context.AccessPoints.Add(d));

                #endregion

                Context.SaveChanges();

                var AccessRoles = new List<IdentityRole>{
                new IdentityRole{
                    Name="Admin"
                },
                new IdentityRole{
                    Name="Field Tech"
                },
                new IdentityRole{
                    Name="Agent"
                }
            };

                AccessRoles.ForEach(d => Context.Roles.Add(d));

                var passwordHasher = new PasswordHasher();

                var userStore = new UserStore<ApplicationUser>(Context);
                var userManager = new UserManager<ApplicationUser>(userStore);

                var Users = new List<ApplicationUser>{
                new ApplicationUser{
                    UserName="kris@kris.com",
                    Email="kris@kris.com",
                    Name ="Kris",
                    SkiffieRoleId = roles.First(x=>x.Name=="Admin").SkiffieRolesId,
                    PasswordHash = passwordHasher.HashPassword("adminpassword!"),
                    SecurityStamp = "abc",
                },
                new ApplicationUser{
                    UserName="tech@tech.com",
                    Email="tech@tech.com",
                    Name="Tech",
                    SkiffieRoleId = roles.First(x=>x.Name=="Field Tech").SkiffieRolesId,
                    PasswordHash = passwordHasher.HashPassword("techPassword"),
                    SecurityStamp = "abc"
                },
                new ApplicationUser{
                    UserName="agent@agent.com",
                    Email="agent@agent.com",
                    Name="Agent",
                    SkiffieRoleId = roles.First(x=>x.Name=="Agent").SkiffieRolesId,
                    PasswordHash = passwordHasher.HashPassword("agentPassword"),
                    SecurityStamp = "abc"
                }
            };

                Users.ForEach(d => Context.Users.Add(d));
                Context.SaveChanges();

                userManager.AddToRole(Users[0].Id, "Admin");
                userManager.AddToRole(Users[1].Id, "Field Tech");
                userManager.AddToRole(Users[2].Id, "Agent");

                Context.SaveChanges();
            }
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
﻿$(document).ready(function () {
    if ($("#AppointmentTable").length > 0) {
        $.ajax({
            url: '/Appointments/GetAppointmentList',
            data: { count: 50 },
            contentType: 'application/html; charset=utf-8',
            type: 'GET',
            dataType: 'html'
        })
        .success(function (result) {
            $("#AppointmentTable").html(result);
        });
    }

    if ($("#PullCustomersOnStartCreate").length > 0) {
        UpdateCustomersCreate();
    }

    $(".filterCustomersAppointment").keydown(function (){
        $("#UpdateCustomerList").trigger("click");
    });

    $(".filterCustomersAppointment").enterKey(function (e) {
        e.preventDefault();
        $("#UpdateCustomerList").trigger("click");
    });

    $("#UpdateCustomerList").click(function () {
        UpdateCustomersCreate();
    });

    $("#TowerId").change(function () {
        getSchedules($(this).val(), $("#ScheduleDiv"));
    });

    $("body").on("click", ".appointmenttimepicker", function () {
        $("#TimeSlotID").val($(this).attr("data-timeslot"));
        $("#Date").val($(this).attr("data-date"));
        if ($("#DateTimeSlot").length > 0) {
            $("#DateTimeSlot").html($(this).attr("data-date") + " " + $(this).attr("data-timeslotname"));
        }

        //alert($(this).attr("data-timeslot") + " " + $(this).attr("data-date"));
    });

    $(".appointmentsFilter").change(function () {
        $("#RefreshTable").trigger("click");
    });

    $("#NextAppointmentPage").click(function () {
        $("#StartAt").val(parseInt($("#StartAt").val(), 10) + 50);
    });

    $("#PreviousAppointmentPage").click(function () {
        $("#StartAt").val(Math.max(parseInt($("#StartAt").val(), 10) - 50, 0));
    });

    $(".appointmentsFilterButton").click(function () {
        $("#RefreshTable").trigger("click");
    });

    $(".appointmentActionButton").click(function () {
        var clicked = $(this);
        if ($("#subView").hasClass(clicked.attr("data-action")) == false) {
            $("#subView").slideUp("fast", function () {
                subViewChange(clicked);
            });
        }
    });

    //now some stuff for the index view.
    $("#RefreshTable").click(function () {
        $.ajax({
            url: '/Appointments/GetAppointmentList',
            data: { count: $("#Amount").val(), skip: $("#StartAt").val(), showOpen: $("#ShowOpen").prop("checked"), showClosed: $("#ShowClosed").prop("checked") },
            contentType: 'application/html; charset=utf-8',
            type: 'GET',
            dataType: 'html'
        })
        .success(function (result) {
            $("#AppointmentTable").html(result);
        });
    });

    $("#AddAppointmentComment").click(function (evt) {
        evt.stopImmediatePropagation();
        if ($("#AppointmentComments").val() != "") {
            $(this).prop('disabled', true);
            $.post("/Appointments/Comment", { id: $("#AppointmentViewModelId").val(), comment: $("#AppointmentComments").val() },
                function (data) {
                    $.get("/Appointments/GetComments", { id: $("#AppointmentViewModelId").val() }, function (x) {
                        $("#comments").html(x);
                    });
                    $("#AppointmentComments").val("");
                }
            );
            $(this).prop('disabled', false);
        }
    });

    $("#SaveAppointmentChangesTech").click(function (evt) {
        $.post("/Customers/UpdateTowerAndIP", $(this.form).serialize(), function (data) {
            if (data['error']) {
                $("#errorsFromServer").show();
                $("#errorsFromServer").html(data['error']);
                $("#messagesFromServer").hide();
            } else if (data['success']){
                $("#messagesFromServer").show();
                $("#messagesFromServer").html(data['success']);
                $("#errorsFromServer").hide();
            }
        });
    });
});

function getSchedules(towerID, htmlTarget) {
    if (towerID == "") {
        htmlTarget.html("Please select a tower or customer to fill in schedule.");
    } else {
        //set the date first. add one to it.
        var Today = new Date();
        var modifiedDate = Today;

        modifiedDate.setDate(Today.getDate() + 1);
        htmlTarget.html("Loading...");
        $.ajax({
            url: '/Appointments/CheckDatesTable',
            data: { Date: modifiedDate.yyyymmdd(), TowerID: towerID, DateCount: 14 },
            contentType: 'application/html; charset=utf-8',
            type: 'GET',
            dataType: 'html'
        })
        .success(function (result) {
            htmlTarget.html(result);
        });
    }
}

function subViewChange(clicked) {
    $(".appointmentActionButton").each(function () {
        $("#subView").removeClass($(this).attr("data-action"));
    });

    $("#subView").addClass(clicked.attr("data-action"));
    $.ajax({
        url: '/Appointments/' + clicked.attr("data-action"),
        data: { id: clicked.attr("data-value") },
        contentType: 'application/html; charset=utf-8',
        type: 'GET',
        dataType: 'html'
    })
    .success(function (result) {
        $("#subView").html(result);
        $("#subView").slideDown("fast");

        //pull the schedule if need be.
        if (clicked.attr("data-action") == "Reschedule") {
            getSchedules($("body #TowerId").val(), $("body #ScheduleDiv"));
        }
    })
    .error(function (xhr, error) {
        alert(error);
        $("#subView").slideDown("fast");
    });
}

function UpdateCustomersCreate(cxid) {
    cxid = cxid || 0;
    $.ajax({
        url: "/Customers/PartialCurrentCustomers",
        data: {
            AppointmentType: $('#AppointmentType').find(":selected").text(), FilterName: $('#FilterName').val(),
        FilterPhone: $('#FilterPhone').val(), CustomerID: cxid},
        contentType: 'application/html; charset=utf-8',
        type: 'GET',
        dataType: 'html'
    })
    .success(function (result) {
        $("#CurrentCustomer").html(result);

        //attach the events.
        $("#CustomerID").keyup(function () {
            $("#CustomerID").trigger("change");
        });

        $("#CustomerID").change(function () {
            var cid = $(this).val();
            if (cid == "") {
                $("#ScheduleDiv").html("Please select a tower or customer to fill in schedule.");
            } else {
                //set the date first. add one to it.
                var Today = new Date();
                var modifiedDate = Today;

                modifiedDate.setDate(Today.getDate() + 1);
                $("#ScheduleDiv").html("Loading...");
                $.ajax({
                    url: '/Appointments/CheckDatesTable',
                    data: { Date: modifiedDate.yyyymmdd(), CustomerID: cid, DateCount: 14 },
                    contentType: 'application/html; charset=utf-8',
                    type: 'GET',
                    dataType: 'html'
                })
                .success(function (result) {
                    $("#ScheduleDiv").html(result);
                });
            }
        });
    });
}

$("body").on('click tap', '.AppointmentRow', function () {
    window.location.href = "/Appointments/View/" + $(this).attr("data-appointmentID");
});
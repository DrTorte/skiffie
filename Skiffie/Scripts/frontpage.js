﻿$(document).ready(function () {
    var Today = new Date();
    if ($("#ScheduleDiv").length > 0) {
        var modifiedDate = Today;
        modifiedDate.setDate(Today.getDate() + 1);
        $.ajax({
            url: '/Appointments/CheckDatesTable',
            data: { Date: modifiedDate.yyyymmdd(), DateCount: 14 },
            contentType: 'application/html; charset=utf-8',
            type: 'GET',
            dataType: 'html'
        })
        .success(function (result) {
            $("#ScheduleDiv").html(result);
        });
    }

    if ($("#UpcomingAppointments").length > 0) {
        $.ajax({
            url: '/Appointments/GetAppointmentList',
            data: { upcomingOnly: true },
            contentType: 'application/html; charset=utf-8',
            type: 'GET',
            dataType: 'html'
        })
        .success(function (result) {
            $("#UpcomingAppointments").html(result);
        });
    }

    if ($("#TowerStatus").length > 0) {
        $.ajax({
            url: '/Towers/GetTowers',
            contentType: 'application/html; charset-utf-8',
            type: 'GET',
            dataType: 'html'
        })
        .success(function (result) {
            $("#TowerStatus").html(result);
        });
    }
});
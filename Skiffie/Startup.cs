﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Skiffie.Startup))]
namespace Skiffie
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
